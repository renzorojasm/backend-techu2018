var express = require('express');
var bodyParser = require('body-parser');
//var usersFile= require('./users.json');
var requestJson=require('request-json');
var app = express();
var URLbase = '/apiperu/v1/';
var port = process.env.PORT || 3000;
var urlMlabRaiz='https://api.mlab.com/api/1/databases/dbapiperu/collections/';
const apiKey='apiKey=TxB_kW_Y8wFHo2zhFsDgd65OZa-sZ0ha';
var clienteMlab;

app.use(bodyParser.json());

//GET users and with query params
app.get (URLbase+'users',function(req,res){
  console.log("GET /apiperu/v1/users - MLAB");
  //oculta el campo del _iD (campo indice de mongodb)
  var queryString='f={"_id":0}&';
  clienteMlab=requestJson.createClient(urlMlabRaiz+'user?'+queryString+apiKey);
  clienteMlab.get('',function(err,resM,body){
    if(!err){
      if(body.length>0){
        res.status(200).send(body);
      }else{
        res.status(204).send();
      }
    }else{
      res.status(409).send(err);
    }
  });
});

//GET users by ID
app.get (URLbase+'users/:id',function(req,res){
  console.log("GET /apiperu/v1/users/"+req.params.id);
  let userId=req.params.id;
  var queryString='q={"userID":'+userId+'}&f={"_id":0}&';
  clienteMlab=requestJson.createClient(urlMlabRaiz+'user?'+queryString+apiKey);
  clienteMlab.get('',function(err,resM,body){
    if(!err){
      if(body.length>0){
        res.status(200).send(body);
      }else{
        res.status(204).send();
      }
    }else{
      res.status(409).send(err);
    }
  });
});


//POST create users
app.post (URLbase+'users/',function(req,res){
  console.log("POST /apiperu/v1/users/");
  var response={};
  var newID=0;
  clienteMlab=requestJson.createClient(urlMlabRaiz);
  var queryString='c=true&';
  clienteMlab.get('user?'+queryString+apiKey,function(err,resL,bodyL){
    if(!err){
      newID=parseInt(bodyL)+1;
      let newUser={
        "userID":newID,
        "first_name":req.body.first_name,
        "last_name":req.body.last_name,
        "email":req.body.email,
        "password":req.body.password
      };
      console.log(newUser);
      clienteMlab.post('user?'+apiKey,newUser,function(err,resP,bodyP){
        if(!err){
          response=bodyP;
          res.status(201);
        }else{
          response=err;
          res.status(500);
        }
        res.send(response);
      });
    }else{
      res.status(500).send(err);
    }
  });
});

//PUT user by ID -  update de un registro especifico
app.put (URLbase+'users/:id',function(req,res){
  console.log("PUT /apiperu/v1/users/");
  let userId=req.params.id;
  console.log(userId);
  var response={};
  var queryString='q={"userID":'+userId+'}&';
  clienteMlab=requestJson.createClient(urlMlabRaiz+'user?'+queryString+apiKey);
  clienteMlab.get('',function(err,resM,bodyG){
    if(!err){
      if(bodyG.length>0){
        var cambio = '{"$set":'+JSON.stringify(req.body)+'}';
        clienteMlab.put('', JSON.parse(cambio), function(errP, resP, bodyP) {
          if(!errP){
            response={
              "msg":"Usuario modificado exitosamente"
            };
            res.status(202).send(response);
          }else{
            response=errP;
            res.status(500).send(response);
          }
        });
      }else{
        response={
          "msg":"Usuario no existe"
        };
        res.status(404).send(response);
      }
    }else{
      response=err;
      res.status(409).send(response);
    }
  });
});


//DELETE user by ID
app.delete (URLbase+'users/:id',function(req,res){
  console.log("DELETE /apiperu/v1/users/");
  let userId=req.params.id;
  var response={};
  clienteMlab=requestJson.createClient(urlMlabRaiz);
  var queryString='q={"userID":'+userId+'}&f={"_id":1}&';
  clienteMlab.get('user?'+queryString+apiKey,function(err,resM,bodyG){
    if(!err){
      if(bodyG.length>0){
        clienteMlab.delete('user/'+bodyG[0]._id.$oid+'?'+apiKey, function(errE, resE, bodyE) {
          if(!errE){
            response={
              "msg":"Usuario eliminado"
            };
            res.status(200).send(response);
          }else{
            response=errE;
            res.status(500).send(response);
          }
        });
      }else{
        response={
          "msg":"Usuario no existe"
        }
        res.status(404).send(response);
      }
    }else{
      res.status(500).send(err);
    }
  });
});

//POST Login
app.post (URLbase+'login',function(req,res){
  var email=req.body.email;
  var pass=req.body.password;
  //query donde se valida el email y passwor
  var queryString='q={"email":'+email+'}&q={"password":'+pass+'}&';
  //creacion del cliente MLAP
  clienteMlab=requestJson.createClient(urlMlabRaiz);
  // para ver si el email existe
  clienteMlab.get('user?'+queryString+apiKey,function(err,resM,bodyG){
    var respuesta=bodyG[0];
    //comprobacion si no hay errores
    if(!err){
      if(respuesta!=undefined){
        if(respuesta.password==pass){
          console.log("Login correcto");
          //se añade una nueva propieda al documento llamada Logged
          var session={"logged":true};
          var login ='{"$set":'+JSON.stringify(session)+'}';
          clienteMlab.put('user?q={"id":'+respuesta.id+'}',JSON.parse(login),function(errL,resL,bodyL){

          });
        }
      }
    }else{
      res.status(404).send("usuario no encontrado")
    }
  });
});

app.listen(port);
